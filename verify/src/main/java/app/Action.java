package app;

public enum Action {
    TRANSFER, WITHDRAWAL;

    public static Action from(String name) {
        for (Action action : Action.values()) {
            if (action.name().equalsIgnoreCase(name)) {
                return action;
            }
        }
        return null;
    }
}
