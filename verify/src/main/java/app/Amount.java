package app;

import java.math.BigInteger;

import static org.apache.commons.lang3.Validate.*;

public class Amount {
    private int value;

    public Amount(String value){
        notNull(value);
        BigInteger parsedVal = new BigInteger(value);
        inclusiveBetween(BigInteger.ZERO, BigInteger.valueOf(10000), parsedVal);
        this.value = parsedVal.intValue();
    }

    public int getValue(){
        return this.value;
    }
}
