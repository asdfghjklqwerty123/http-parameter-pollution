package app;

import app.exception.BadRequestException;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.client.RestTemplate;

import javax.servlet.http.HttpServletRequest;

@RestController
public class VerifyController {

    @Value("${paymenturl}")
    private String paymentUrl;

    @PostMapping("/")
    public String res(HttpServletRequest request) {
        try {
            Action action = Action.from(request.getParameter("action"));
            Amount amount = new Amount(request.getParameter("amount"));
            switch(action){
                case TRANSFER:
                    System.out.println("Verify Controller: Going to transfer $"+amount);
                    RestTemplate restTemplate = new RestTemplate();
                    String fakePaymentUrl = this.paymentUrl;  //Internal fake payment micro-service
                    ResponseEntity<String> response
                            = restTemplate.getForEntity(
                            fakePaymentUrl + "?action=" + action.toString() + "&amount=" + amount.getValue(),
                            String.class);
                    return response.getBody();
                case WITHDRAWAL:
                    return "Verify Controller: Sorry, you can only make transfer";
                default:
                    throw new BadRequestException();
            }
        } catch(RuntimeException ex) {
            throw new BadRequestException();
        }
    }

}
